package clienteudp;

import java.awt.Dialog;
import java.awt.Font;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.*;

/**
 *
 * @author Mariano-Larios
 */
public class ClienteUDP extends JFrame{
    private javax.swing.JLabel eti;
    public ClienteUDP(){
        
        setSize(200,100);  
        try{
            setUndecorated(true);
        }catch(IllegalComponentStateException e){e.printStackTrace();}
        eti=new javax.swing.JLabel();
        eti.setFont(new Font("Verdana", Font.BOLD, 20));
        getContentPane().add(eti);


        Thread t = new Thread(new Runnable(){
            public void run(){
                int hora,min,seg;
                while(true){
                    
                    /*Como ejemplo utilizo el objeto Calendario pra asimular
                    la obtencion de timepo que el servidor me debe proporcionar*/
                    //  No olvidar utilizar la sincronizacion para uno o más clientes
                    //En la linea 43 a la 47 se exribe otra forma de realizar la sincronización
                    synchronized(this){
                    Calendar calendario = Calendar.getInstance();
                    hora =calendario.get(Calendar.HOUR_OF_DAY);
                    min = calendario.get(Calendar.MINUTE);
                    seg = calendario.get(Calendar.SECOND);
                    eti.setText(hora+":"+min+":"+seg);
                    }
/*               try{
                    Thread.sleep(1000);
                }catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();}*/
                }
            }
        });
        t.start();
        
    }
    public static void main(String[] args) {
        ClienteUDP fr = new ClienteUDP();
        SwingUtilities.invokeLater(new Runnable() {
        public void run() {
            fr.setVisible(true);
            fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        });
    }
    
}
