package servidorrmi;

import java.awt.GridLayout;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import java.rmi.*;
import java.net.MalformedURLException;
import java.rmi.server.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.io.*;
import java.net.*;



public class ServidorRMI extends JFrame {
    private JScrollPane jsp;
    private JScrollPane jspLista;
    private JTextArea area;
    private JTextArea areaLista;
    private int puerto,hora,min,seg;
    private final int MAX_LON=20;
    private String mensajeE,mensajeR;	
    private String registryURL;
    private String makinaRemota;
    public ServidorRMI(){
        setSize(500,300); 
        puerto=8080;
	mensajeE="";
        mensajeR="";
        MisComponentes();
    }
    
    private void MisComponentes(){        
        area = new JTextArea();
        jsp = new JScrollPane(area);
        areaLista = new JTextArea();
        jspLista = new JScrollPane(areaLista);
        getContentPane().setLayout(new GridLayout());
        getContentPane().add(jsp);  
        getContentPane().add(jspLista);        
        int RMIPortNum=puerto;
        area.append("Servidor \n");
        area.append("RMIregistry Puerto: "+RMIPortNum+"\n");
        try{           
                startRegistry(RMIPortNum);
                CronoImpl exportObj =new CronoImpl();
                //registryURL="rmi://"+makinaRemota+":"+puerto+"/crono";
                registryURL="rmi://localhost:"+puerto+"/crono";
                Naming.rebind(registryURL,exportObj);
                area.append("Server registred. Registro contiene: \n");
                listRegistry(registryURL);
        }catch(Exception e){e.printStackTrace();}
            area.append("Hello Server ready \n");
            areaLista.append("Clientes \n");
            Thread t = new Thread(new Runnable(){
                public void run(){
                    try{
                    String[] names=Naming.list(registryURL);
                    for(int i=0;i<names.length;i++)
			areaLista.append(names[i]+"\n");
                        Thread.sleep(100);
                    }catch(InterruptedException | RemoteException e){}
                    catch(MalformedURLException e){}
                }
            });
            t.start();
	}
	private static void startRegistry(int RMIPortNum)throws RemoteException{
            try{
		Registry registry=LocateRegistry.createRegistry(RMIPortNum);
		registry.list();
            }catch(RemoteException ex){}
	}
	private  void listRegistry(String registryURL)
	throws RemoteException, MalformedURLException{
		area.append("Registro : \n");
                area.append(registryURL+" contiene: \n");
		String[] names=Naming.list(registryURL);
		for(int i=0;i<names.length;i++)
			area.append(names[i]+"\n");
        }
        public static void main(String[] args) {
            ServidorRMI fr = new ServidorRMI();
            SwingUtilities.invokeLater(new Runnable(){
                public void run(){
                    fr.setVisible(true);
                    fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            });
        }
}
