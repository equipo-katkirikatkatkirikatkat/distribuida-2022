package servidorudp;
import java.awt.IllegalComponentStateException;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.util.Calendar;

public class ServidorUDP extends javax.swing.JFrame {
    private JScrollPane jsp;
    private JTextArea area;
    private int puerto,hora,min,seg;
    private final int MAX_LON=20;
    private String mensajeE,mensajeR;	
    
    public ServidorUDP(){
        setSize(200,100); 
        puerto=8080;
	mensajeE="";
        mensajeR="";
        MisComponentes();
    }
    
    private void MisComponentes(){        
        area= new JTextArea();
        jsp=new JScrollPane(area);
        getContentPane().setLayout(new GridLayout());
        getContentPane().add(jsp);

        Thread tEmisor = new Thread(new Runnable(){
            public void run(){
             Calendar calendario = Calendar.getInstance();
            hora =calendario.get(Calendar.HOUR_OF_DAY);
            min = calendario.get(Calendar.MINUTE);
            seg = calendario.get(Calendar.SECOND);
            mensajeE = hora+":"+min+":"+seg;
                    try{
			InetAddress makinaRemota=InetAddress.getLocalHost();
			int puerto=8080;
			DatagramSocket miSocket=new DatagramSocket();
			byte[] almacen=mensajeE.getBytes();
			DatagramPacket datagrama=new DatagramPacket(almacen,almacen.length,
			makinaRemota,puerto);
			miSocket.send(datagrama);
			miSocket.close();
		}catch(Exception ex){ex.printStackTrace();}
            }
        });
        tEmisor.start();
        Thread tReceptor = new Thread(new Runnable(){
	public void run(){
		while(true)
			try{
				DatagramSocket miSocket=new DatagramSocket(puerto);
				byte[] almacen=new byte[MAX_LON];
				DatagramPacket datagrama=new DatagramPacket(almacen,MAX_LON);
				miSocket.receive(datagrama);
				mensajeR=new String(almacen);
				area.append(mensajeR +"\n");
				System.out.println(mensajeR);
				miSocket.close();
			}catch(Exception ex){ex.printStackTrace();}
	}

        });
        tReceptor.start();
    }
    public static void main(String[] args) {
        ServidorUDP fr = new ServidorUDP();
        SwingUtilities.invokeLater(new Runnable(){
            public void run(){
                fr.setVisible(true);
                fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });
    }
    
}
